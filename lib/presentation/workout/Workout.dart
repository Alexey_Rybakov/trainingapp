import 'package:flutter/material.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';

import '../navigation.dart';
import 'WorkoutController.dart';
import 'enter_exe/WorkoutInput.dart';

class WorkoutState
    extends ScreenStateWithController<WorkoutController, WorkoutScreen> {
  @override
  WorkoutController provideController() {
    return WorkoutController(Repository(), widget.date);
  }

  bool isFinished(AsyncSnapshot<AsyncResult<WorkoutDay>> snapshot) {
    AsyncResult<WorkoutDay> result = snapshot.data;
    if (snapshot.hasData) {
      if (result is Data<WorkoutDay>) {
        var data = result.data;
        return data.finished;
      }
    }
    return false;
  }

  bool isNotFinished(AsyncSnapshot<AsyncResult<WorkoutDay>> snapshot) {
    AsyncResult<WorkoutDay> result = snapshot.data;
    if (snapshot.hasData) {
      if (result is Data<WorkoutDay>) {
        var data = result.data;
        return !data.finished;
      }
    }
    return false;
  }

  Widget layout(
      BuildContext context, AsyncSnapshot<AsyncResult<WorkoutDay>> snapshot) {
    AsyncResult<WorkoutDay> result = snapshot.data;
    if (snapshot.hasData) {
      if (result is Loading) {
        return SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Center(child: CircularProgressIndicator()));
      } else if (result is Error) {
        return Text('${snapshot.error}');
      } else if (result is Data<WorkoutDay>) {
        var data = result.data;
        return content(data);
      }
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AsyncResult<WorkoutDay>>(
        stream: controller.state,
        builder: (context, snapshot) {
          return Scaffold(
              appBar: AppBar(
                title: Text('Тренировка'),
                actions: <Widget>[
                  if (isFinished(snapshot))
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        controller.onAction();
                      },
                    ),
                  if (isNotFinished(snapshot))
                    IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () {
                          controller.onAction();
                        })
                ],
              ),
              body: layout(context, snapshot));
        });
  }

  Widget content(WorkoutDay data) {
    return exercises(header(data), data, data.workExercises);
  }

  Widget header(WorkoutDay data) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              data.title,
              style: Theme.of(context).textTheme.title,
              textAlign: TextAlign.start,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Дата',
                style: TextStyle(color: categoryColor),
              )),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(Instant.fromEpochMilliseconds(data.time)
                  .inLocalZone()
                  .calendarDate
                  .toString())),
        ),
      ],
    );
  }

  Widget exercises(
      Widget header, WorkoutDay workoutDay, List<WorkExercise> data) {
    return ReorderableListView(
      header: header,
      children: data.map((program) => item(workoutDay, program)).toList(),
      onReorder: (int oldIndex, int newIndex) =>
          {controller.onExerciseReordered(oldIndex, newIndex)},
    );
  }

  Widget item(WorkoutDay workoutDay, WorkExercise item) {
    Widget leading;

    if (item.exercise.image != null) {
      leading = Image.asset(item.exercise.image, height: 48, width: 48);
    }

    return ListTile(
      key: ObjectKey(item.id),
      isThreeLine: false,
      leading: leading,
      trailing: Icon(Icons.drag_handle),
      onTap: () => {
            Navigator.of(context).pushNamed(RouteNames.enterExe,
                arguments: WorkoutInputArgument(workoutDay, item))
          },
      enabled: true,
      title: Text(item.exercise.name, maxLines: 1),
      subtitle: Text(item.strategy, maxLines: 2),
    );
  }

  Table table() {
    return Table(
        border: TableBorder.all(
            color: Colors.blue, width: 1.0, style: BorderStyle.solid),
        children: [
          TableRow(children: [
            TableCell(
              child: Text('№'),
            ),
            TableCell(
              child: Text('Вес, кг'),
            ),
            TableCell(
              child: Text('Повторения'),
            )
          ])
        ]);
  }
}

class WorkoutScreen extends StatefulWidget {
  final LocalDate date;

  WorkoutScreen(this.date);

  @override
  State<StatefulWidget> createState() {
    return WorkoutState();
  }
}
