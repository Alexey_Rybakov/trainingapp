

import 'package:rxdart/rxdart.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/workout/enter_exe/WorkoutInput.dart';

class WorkExerciseController extends LoadingController<WorkExercise>{

  Repository _repository;
  WorkExercise argument;

  WorkExerciseController(this._repository, this.argument);

  @override
  void onCreate() async{
    super.onCreate();
  }



  @override
  Stream<WorkExercise> dataStream() {
    return Stream.fromIterable([argument]);
  }

}
