import 'package:flutter/foundation.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

import '../Bus.dart';
import '../navigation.dart';

class ScheduleController extends LoadingController<ScheduleModel> {
  Repository _repository;

  ScheduleController(this._repository);

  @override
  Stream<ScheduleModel> dataStream() {
    return _repository.workoutDays().asStream().map((data) {
      return ScheduleModel(data);
    });
  }

  @override
  Stream<void> refreshStream() {
    return Bus.refreshWorkoutDays;
  }

  @override
  void onCreate() {
    super.onCreate();
    state.listen((event) => {debugPrint('Event: $event')});
  }

  void onDayClicked(LocalDate date) async {
    var snapshot = await state.first;
    if (snapshot is Data<ScheduleModel>) {
      ScheduleModel scheduleModel = snapshot.data;
      var data = scheduleModel.daysByDate[date];
      if(data != null)
      navigation.sink.add(NavEvent(NavType.PUSH, RouteNames.workout, arguments: date));
    }
  }
}

class ScheduleModel {
  List<WorkoutDay> days;
  Map<LocalDate, WorkoutDay> daysByDate;

  ScheduleModel(this.days) {
    daysByDate = Map.fromIterable(days,
        key: (e) => Instant.fromEpochMilliseconds(e.time).inLocalZone().calendarDate,
        value: (e) => e);
  }
}
