

import 'package:rxdart/rxdart.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

import '../Bus.dart';

class WorkoutController extends LoadingController<WorkoutDay>{

  Repository _repository;
  LocalDate date;

  WorkoutController(this._repository, this.date);


  @override
  Stream<WorkoutDay> dataStream() {
    return _repository.workoutDays(date: date).asStream().map((list){
      if(list.isNotEmpty){
        return list.first;
      }else{
        return null;
      }
    });
  }

  void onAction() async{
    var snapshot = await dataStream().first;
    snapshot.finished = !snapshot.finished;
    _repository.updateWorkoutDay(snapshot);
    stateSubject.add(AsyncResult.data(snapshot));
    Bus.refreshWorkoutDays.add(null);
  }

  void onExerciseReordered(int oldIndex, int newIndex) async {
    final snapshot = await state.first;
    if (snapshot is Data<Program>) {
      /*final program = snapshot.data;
      List<WorkExercise> list = List.of(program.workExercises);
      list.insert(newIndex, list.removeAt(oldIndex));
      stateSubject
          .add(AsyncResult.data(Program.clone(program, workExercises: list)));*/
    }
  }

}
