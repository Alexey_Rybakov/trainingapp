import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/domain/models.dart';

const String TABLE_WorkoutDay = 'WorkoutDay';
const String TABLE_PROGRAM = 'program';
const String TABLE_EXERCISE = 'exersice';

class SqlDatabase {
  Database _database;

  Future _initIfNeeded() async {
    if (_database != null) return;
    Directory directory = await getApplicationDocumentsDirectory();
    var path = join(directory.path, 'database');

    _database = await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table

      await db.execute(
          'CREATE TABLE Test (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, value INTEGER, num REAL);');

      var batch = db.batch();
      batch.execute(
          'CREATE TABLE $TABLE_EXERCISE (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, muscle TEXT, description TEXT);');
      batch.execute(
          'CREATE TABLE $TABLE_WorkoutDay (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, time INTEGER, data BLOB);');
      batch.execute(
          'CREATE TABLE $TABLE_PROGRAM (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, data BLOB);');

      Bundled().setup(batch);
      await batch.commit();
    });
  }

  Future<List<Map<String, dynamic>>> runSQL(String sql) async {
    await _initIfNeeded();
    return await _database.rawQuery(sql);
  }

  Future updateWorkoutDay(WorkoutDay workoutDay) async {
    await _initIfNeeded();
    _database.update(TABLE_WorkoutDay,
        {'time': workoutDay.time, 'data': json.encode(workoutDay.toJson())},
        where: 'id = ?', whereArgs: [workoutDay.id]);
  }

  Future addWorkoutDay(WorkoutDay workoutDay) async {
    await _initIfNeeded();
    _database.execute(
        '''INSERT INTO $TABLE_WorkoutDay (time, data) VALUES (?, ?);''',
        [workoutDay.time, json.encode(workoutDay.toJson())]);
  }

  Future<List<WorkoutDay>> workoutDays({LocalDate localDate = null}) async {
    await _initIfNeeded();
    List<Map<String, dynamic>> query;
    if (localDate == null) {
      query = await _database
          .query(TABLE_WorkoutDay, columns: ['id', 'time', 'data']);
    } else {
      var instantMs =
          localDate.atMidnight().toDateTimeLocal().millisecondsSinceEpoch;
      query = await _database.query(TABLE_WorkoutDay,
          columns: ['id', 'time', 'data'],
          where: 'time = ?',
          whereArgs: [instantMs]);
    }
    List<WorkoutDay> workoutDays = query.map((e) {
      var day = WorkoutDay.fromJson(json.decode(e['data']));
      day.id = e['id'];
      return day;
    }).toList();

    return workoutDays;
  }

  Future<List<Map<String, dynamic>>> listOfExercises() async {
    await _initIfNeeded();

    return _database.query("Test", columns: ['id', 'name', 'value', 'num']);
  }

  Future<List<Program>> programs() async {
    await _initIfNeeded();
    List<Map<String, dynamic>> query;
    query = await _database.query(TABLE_PROGRAM, columns: ['id', 'data']);
    return query.map((e) {
      var program = Program.fromJson(json.decode(e['data']));
      program.id = e['id'];
      return program;
    }).toList();
  }

  Future updateProgram(Program program) async {
    await _initIfNeeded();
    _database.update(TABLE_PROGRAM, {'data': json.encode(program.toJson())},
        where: 'id = ?', whereArgs: [program.id]);
  }
}

class Bundled {
  void setup(Batch batch) {
    _workoutDays().forEach((e) {
      batch.execute(
          '''INSERT INTO $TABLE_WorkoutDay (time, data) VALUES (?, ?);''',
          [e.time, json.encode(e.toJson())]);
    });
    _getPrograms().forEach((e) {
      batch.execute('''INSERT INTO $TABLE_PROGRAM (data) VALUES (?);''',
          [json.encode(e.toJson())]);
    });
  }

  List<WorkoutDay> _workoutDays() {
    LocalDate today = LocalDateTime.now().calendarDate;
    return [
      WorkoutDay(
          null,
          'Title',
          today
              .subtractDays(10)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ]),
      WorkoutDay(
          null,
          'Title',
          today
              .subtractDays(8)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ],
          finished: true),
      WorkoutDay(
          null,
          'Title',
          today
              .subtractDays(5)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ]),
      WorkoutDay(
          null,
          'Title',
          today
              .subtractDays(3)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ]),
      WorkoutDay(
          null,
          'Title',
          today
              .addDays(1)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ]),
      WorkoutDay(
          null,
          'Title',
          today
              .addDays(4)
              .atMidnight()
              .toDateTimeLocal()
              .millisecondsSinceEpoch,
          [
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ]),
            WorkExercise(
                1,
                "3*12 среднее напряжение",
                Exercise(0, "name1", "descr1", "category",
                    image: 'assets/graphics/stub.png'),
                [
                  WorkSet(1, 10, 40),
                  WorkSet(2, 10, 40),
                  WorkSet(3, 10, 40),
                  WorkSet(4, 10, 40),
                  WorkSet(5, 10, 40),
                ])
          ])
    ];
  }

  List<Program> _getPrograms() {
    return [
      Program(1, "Программа для новичков", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ]),
      Program(2, "Быстрый старт", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ]),
      Program(3, "Фулбади", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ]),
      Program(4, "Сплит на 3 дня", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ]),
      Program(5, "Сплит на 4 дня", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ]),
      Program(6, "Классическая", "Описание", [
        WorkExercise(
            1,
            "3*12 среднее напряжение",
            Exercise(0, "name1", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            3,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            4,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ]),
        WorkExercise(
            5,
            "3*12 среднее напряжение",
            Exercise(0, "name3", "descr1", "category",
                image: 'assets/graphics/stub.png'),
            [
              WorkSet(1, 10, 40),
              WorkSet(2, 10, 40),
              WorkSet(3, 10, 40),
              WorkSet(4, 10, 40),
              WorkSet(5, 10, 40),
            ])
      ])
    ];
  }
}
