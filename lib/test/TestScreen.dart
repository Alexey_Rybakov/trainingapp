import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:training/data/Database.dart';

class TestState extends State<TestScreen> {
  SqlDatabase _database;

  TextEditingController _textEditingController = TextEditingController();
  String sqlResult = "";

  @override
  void initState() {
    _database = SqlDatabase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.only(top: 40, left: 16, right: 16),
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TextField(
                  obscureText: false,
                  controller: _textEditingController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Sql query',
                  ),
                ),
                RaisedButton(
                    onPressed: () {
                      runSql(_textEditingController.value.text);
                    },
                    child: Text('Run query', style: TextStyle(fontSize: 20))),
                Text(sqlResult)
              ],
            ),
          )
        ],
      ),
    ));
  }

  void runSql(String request) async {
    try {
      var result = await _database.runSQL(_textEditingController.value.text);
      setState(() {
        var encoder = new JsonEncoder.withIndent("     ");
        sqlResult = encoder.convert(result);
      });
    } catch (e) {
      setState(() {
        sqlResult = e.toString();
      });
    }
  }
}

class TestScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TestState();
  }
}
