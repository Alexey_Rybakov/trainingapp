

import 'package:rxdart/rxdart.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

class ExerciseDetailedController extends LoadingController<Exercise>{

  Repository _repository;
  Exercise exercise;

  ExerciseDetailedController(this._repository, this.exercise);

  @override
  void onCreate() async{
    super.onCreate();
  }

  @override
  Stream<Exercise> dataStream() {
    return Stream.fromIterable([exercise]);
  }

}
