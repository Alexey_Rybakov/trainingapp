import 'package:flutter/widgets.dart';
import 'package:time_machine/time_machine.dart';

int monthWeekCount(LocalDate date) {
  final month = startOfMonth(date);
  LocalDate startOfTheCurrentWeek = startOfWeek(month);

  int weekCount = 0;

  while ((startOfTheCurrentWeek.year == month.year &&
          startOfTheCurrentWeek.monthOfYear <= month.monthOfYear) ||
      startOfTheCurrentWeek.year < month.year) {
    weekCount++;
    startOfTheCurrentWeek = startOfTheCurrentWeek.addWeeks(1);
  }
  return weekCount;
}

List<List<LocalDate>> weeklyMonthOf(LocalDate date) {
  debugPrint('weeklyMonthOf: $date');
  final month = startOfMonth(date);

  final monthWeeks = List<List<LocalDate>>();

  LocalDate startOfTheCurrentWeek = startOfWeek(month);

  while ((startOfTheCurrentWeek.year == month.year &&
          startOfTheCurrentWeek.monthOfYear <= month.monthOfYear) ||
      startOfTheCurrentWeek.year < month.year) {
    monthWeeks.add(weekOf(startOfTheCurrentWeek));
    startOfTheCurrentWeek = startOfTheCurrentWeek.addWeeks(1);
  }

  return monthWeeks;
}

List<LocalDate> weekOf(LocalDate date) {
  debugPrint('WeekOf: $date');
  LocalDate startOfTheCurrentWeek = startOfWeek(date);
  return [
    startOfTheCurrentWeek,
    startOfTheCurrentWeek.addDays(1),
    startOfTheCurrentWeek.addDays(2),
    startOfTheCurrentWeek.addDays(3),
    startOfTheCurrentWeek.addDays(4),
    startOfTheCurrentWeek.addDays(5),
    startOfTheCurrentWeek.addDays(6),
  ];
}

LocalDate startOfWeek(LocalDate date) {
  return date.subtractDays(date.dayOfWeek.value - 1);
}

LocalDate startOfMonth(LocalDate date) {
  return LocalDate(date.year, date.monthOfYear, 1, date.calendar, date.era);
}
