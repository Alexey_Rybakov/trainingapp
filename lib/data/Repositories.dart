
import 'package:time_machine/time_machine.dart';
import 'package:training/domain/models.dart';

abstract class ProgramRepository{

  Future<List<Program>> allPrograms();

  Future addProgram(Program program);

  Future removeProgram(Program program);

}


///WorkoutDay defines workout in terms of program
///There may be several workout for certain date
abstract class ScheduleRepository{

  Future<List<WorkoutDay>> workouts({LocalDate date});

  Future addWorkout(WorkoutDay workout);

  Future removeWorkout(WorkoutDay workout);

}

abstract class ExerciseRepository{

  Future<List<Exercise>> exercises();

  Future removeExercise(Exercise exercise);

  Future addExercise(Exercise exercise);

}