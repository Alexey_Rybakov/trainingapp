import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';
import 'package:training/presentation/exercises/ExersiseList.dart';
import 'package:training/presentation/navigation.dart';
import 'package:training/presentation/program/ProgramController.dart';

import 'schedule_program/ScheduleProgram.dart';

class ProgramState
    extends ScreenStateWithController<ProgramController, ProgramScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Программа'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.calendar_today),
              onPressed: () {
                Navigator.of(context).pushNamed(RouteNames.scheduleProgram, arguments: widget.program);
              },
              tooltip: 'Запланировать',
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            pickExerciseId();
          },
          tooltip: 'Добавить упражнение',
          child: Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        body: StreamBuilder<AsyncResult<Program>>(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<Program> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<Program>) {
                  var data = result.data;
                  return exercises(content(data), data.workExercises);
                }
              }
              return Container();
            }));
  }

  Widget content(Program data) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              data.name,
              style: Theme.of(context).textTheme.title,
              textAlign: TextAlign.start,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Описание'.toUpperCase(),
                style: TextStyle(color: categoryColor),
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Align(
              alignment: Alignment.centerLeft, child: Text(data.description)),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Упражнения'.toUpperCase(),
                style: TextStyle(color: categoryColor),
              )),
        )
      ],
    );
  }

  Widget exercises(Widget header, List<WorkExercise> data) {
    return ReorderableListView(
      header: header,
      children: data.map((program) => item(program)).toList(),
      onReorder: (int oldIndex, int newIndex) =>
          {controller.onExerciseReordered(oldIndex, newIndex)},
    );
  }

  Widget item(WorkExercise item) {
    Widget leading;

    if (item.exercise.image != null) {
      leading = Image.asset(item.exercise.image, height: 48, width: 48);
    }

    return ListTile(
        key: ObjectKey(item.id),
        isThreeLine: false,
        leading: leading,
        trailing: Icon(Icons.drag_handle),
        onTap: () => {Navigator.of(context).pushNamed(RouteNames.workExercise, arguments: item)},
        enabled: true,
        title: Text(item.exercise.name, maxLines: 1),
        subtitle: Text(item.strategy, maxLines: 2));
  }

  @override
  ProgramController provideController() {
    return ProgramController(Repository(), widget.program.id);
  }

  void pickExerciseId() async {
    final result = await Navigator.of(context).push<int>(
        MaterialPageRoute(builder: (context) => ExerciseListPickerScreen()));
    controller.addExercise(result);
  }
}

class ProgramScreen extends StatefulWidget {
  final Program program;

  ProgramScreen(this.program);

  @override
  State<StatefulWidget> createState() {
    return ProgramState();
  }
}
