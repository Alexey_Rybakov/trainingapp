import 'package:flutter/foundation.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

class ProgramsListController extends LoadingController<List<Program>> {
  Repository _repository;

  ProgramsListController(this._repository);

  @override
  Stream<List<Program>> dataStream() {
    return _repository.getPrograms().asStream().map((data) {
      return data;
    });
  }

  @override
  void onCreate() {
    super.onCreate();
    state.listen((event) => {debugPrint('Event: $event')});
  }
}
