
import 'package:time_machine/time_machine.dart';
import 'package:training/DateTimeUtils.dart';

///There is still no scrollToIndex method in flutter, so use
///manually calculation for scroll to pixel offset
class ScrollCalculator{

  double _monthHeaderHeight;
  double _monthWeekHeight;
  LocalDate _rangeFrom;
  LocalDate _rangeTo;

  List<int> _weeksCountForMonth = List();

  ScrollCalculator(this._monthHeaderHeight, this._monthWeekHeight, this._rangeFrom, this._rangeTo){

    LocalDate currentMonth = startOfMonth(_rangeFrom);
    LocalDate rangeToMonth = startOfMonth(_rangeTo);

    while ((currentMonth.year == rangeToMonth.year &&
        currentMonth.monthOfYear <= rangeToMonth.monthOfYear) ||
        currentMonth.year < rangeToMonth.year){
      _weeksCountForMonth.add(monthWeekCount(currentMonth));
      currentMonth = currentMonth.addMonths(1);
    }

  }

  double calculateScrollToMonth(int scrollTo){
      double offset = 0;
      for(int i = 0; i < scrollTo; i++){
        offset += _monthHeaderHeight;
        offset += _weeksCountForMonth[i] * _monthWeekHeight;
      }
      return offset;
  }
}

