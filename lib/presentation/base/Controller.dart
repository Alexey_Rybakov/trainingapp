import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

abstract class ScreenStateWithController<C extends Controller,
    W extends StatefulWidget> extends State<W> {
  C controller;

  C provideController();

  @override
  void initState() {
    controller = provideController();
    controller.onCreate();
    controller.navigation.controller.stream.listen((event) {
      if (event.navType == NavType.PUSH) {
        Navigator.of(context)
            .pushNamed(event.route, arguments: event.arguments);
      } else if (event.navType == NavType.BACK) {
        Navigator.of(context).pop();
      } else {}
    });
    //Navigator.of(context).pushNamed(RouteNames.program)
    super.initState();
  }
}

abstract class AsyncResult<T> {
  factory AsyncResult.data(T data) {
    return Data._(data);
  }

  factory AsyncResult.loading() {
    return Loading._();
  }

  factory AsyncResult.error(Exception exception) {
    return Error._(exception);
  }
}

class Data<T> implements AsyncResult<T> {
  final T data;

  Data._(this.data);
}

class Error<T> implements AsyncResult<T> {
  final Exception exception;

  Error._(this.exception);
}

class Loading<T> implements AsyncResult<T> {
  const Loading._();
}

abstract class LoadingController<D> extends Controller {
  @protected
  BehaviorSubject<AsyncResult<D>> stateSubject =
      BehaviorSubject.seeded(AsyncResult.loading());

  Stream<AsyncResult<D>> get state => stateSubject;

  @protected
  Stream<D> dataStream();

  @protected
  Stream<void> refreshStream() {
    return Stream.empty();
  }

  void onCreate() async {
    stateSubject.add(AsyncResult.loading());
    stateSubject.addStream(dataStream().map((data) {
      return AsyncResult.data(data);
    }));

    refreshStream().listen((void data) {
      stateSubject.addStream(dataStream().map((data) {
        return AsyncResult.data(data);
      }));
    });
  }

  void refresh() {
    stateSubject.addStream(dataStream().map((data) {
      return AsyncResult.data(data);
    }));
  }
}

enum NavType { PUSH, ROOT, BACK }

class NavEvent {
  NavType navType;
  String route;
  Object arguments;

  NavEvent(this.navType, this.route, {this.arguments});
}

abstract class Controller {
  PublishSubject<NavEvent> navigation = PublishSubject();

  void onCreate();
}
