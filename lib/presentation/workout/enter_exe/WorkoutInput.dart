import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';

import 'WorkoutInputController.dart';

///Use ref comparison to determine current workExercise to modify WorkoutDay
///There is no Exercise table or id yet
class WorkoutInputArgument {
  WorkoutDay workoutDay;
  WorkExercise workExercise;

  WorkoutInputArgument(this.workoutDay, this.workExercise);
}

class WorkoutInputState extends ScreenStateWithController<
    WorkoutInputController, WorkoutInputScreen> {
  @override
  WorkoutInputController provideController() {
    return WorkoutInputController(Repository(), widget.argument);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Вып. упражнения'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            WorkSet workSet = controller.createNewSet();
            changeSetDialog(workSet);
          },
          child: Icon(Icons.add),
        ),
        body: StreamBuilder<AsyncResult<WorkExercise>>(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<WorkExercise> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<WorkExercise>) {
                  var data = result.data;
                  return content(data);
                }
              }
              return Container();
            }));
  }

  Widget content(WorkExercise data) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                data.exercise.name,
                style: Theme.of(context).textTheme.title,
                textAlign: TextAlign.start,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Стратегия',
                  style: TextStyle(color: categoryColor),
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft, child: Text(data.strategy)),
          ),
          table(data.sets)
        ],
      ),
    );
  }

  Widget clickableCell(String text, void callback()) {
    return InkWell(
        onTap: () {
          callback();
        },
        child: cell(text));
  }

  Widget cell(String text) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Text(text, textAlign: TextAlign.center),
    );
  }

  Widget table(List<WorkSet> sets) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          border: TableBorder.all(
              color: Colors.blue, width: 1.0, style: BorderStyle.solid),
          children: [
            TableRow(children: [
              TableCell(
                child: cell('№'),
              ),
              TableCell(
                child: cell('Вес, кг'),
              ),
              TableCell(
                child: cell('Повторения'),
              ),
              TableCell(
                child: cell(''),
              )
            ]),
            for (var i = 0; i < sets.length; i++)
              TableRow(children: [
                TableCell(
                  child: clickableCell('$i', () {}),
                ),
                TableCell(
                  child: clickableCell('${sets[i].weight}', () {}),
                ),
                TableCell(
                  child: clickableCell('${sets[i].reps}', () {}),
                ),
                TableCell(
                    child: InkWell(
                        onTap: () {
                          changeSetDialog(sets[i]);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.edit,
                            size: 12,
                          ),
                        )))
              ])
          ]),
    );
  }

  TextEditingController _dialogRepsInput = TextEditingController();
  TextEditingController _dialogWeightInput = TextEditingController();

  void changeSetDialog(WorkSet set) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Редактирование',
              style: TextStyle(fontSize: 20),
            ),
            actions: <Widget>[
              MaterialButton(
                child: Text('Удалить',
                    style: TextStyle(color: Theme.of(context).accentColor)),
                onPressed: () {
                  controller.removeSet(set);
                  Navigator.of(context).pop();
                },
              ),
              MaterialButton(
                child: Text(
                  'Сохранить',
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
                onPressed: () {
                  controller.updateSet(_dialogRepsInput.value.text,
                      _dialogWeightInput.value.text, set);
                  Navigator.of(context).pop();
                },
              ),
            ],
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: _dialogRepsInput,
                    keyboardType: TextInputType.number,
                    obscureText: false,
                    decoration:
                        InputDecoration(labelText: 'Количество повторений'),
                  ),
                  TextField(
                    controller: _dialogWeightInput,
                    keyboardType: TextInputType.number,
                    obscureText: false,
                    decoration: InputDecoration(labelText: 'Вес'),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

class WorkoutInputScreen extends StatefulWidget {
  final WorkoutInputArgument argument;

  WorkoutInputScreen(this.argument);

  @override
  State<StatefulWidget> createState() {
    return WorkoutInputState();
  }
}
