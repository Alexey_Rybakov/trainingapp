import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';
import 'package:training/presentation/exercise/ExerciseDetailedController.dart';

class ExerciseDetailedState extends ScreenStateWithController<
    ExerciseDetailedController, ExerciseDetailedScreen> {
  @override
  ExerciseDetailedController provideController() {
    return ExerciseDetailedController(Repository(), widget.exercise);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Упражнение'),
        ),
        body: StreamBuilder<AsyncResult<Exercise>>(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<Exercise> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<Exercise>) {
                  var data = result.data;
                  return content(data);
                }
              }
              return Container();
            }));
  }

  Widget content(Exercise data) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                data.name,
                style: Theme.of(context).textTheme.title,
                textAlign: TextAlign.start,
              ),
            ),
          ),
          AspectRatio(
              aspectRatio: 3 / 1,
              child: Image.asset('assets/graphics/stub.png',
                  height: 48, width: 48)),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Описание'.toUpperCase(),
                  style: TextStyle(color: categoryColor),
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(data.description)
            ),
          )
        ],
      ),
    );
  }
}

class ExerciseDetailedScreen extends StatefulWidget {

  final Exercise exercise;

  ExerciseDetailedScreen(this.exercise);

  @override
  State<StatefulWidget> createState() {
    return ExerciseDetailedState();
  }
}
