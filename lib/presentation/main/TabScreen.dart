import 'package:flutter/material.dart';
import 'package:training/presentation/exercises/ExersiseList.dart';
import 'package:training/presentation/programs/ProgramsList.dart';
import 'package:training/presentation/schedule/Schedule.dart';

import '../navigation.dart';

class TabScreenState extends State<TabScreen> {
  int _currentIndex = 0;

  Widget _pageByIndex(int index) {
    switch (index) {
      case 0:
        return ScheduleScreen();
      case 1:
        return ProgramsListScreen();
      case 2:
        return ExerciseListScreen("Упражнения");
    }
    throw Exception('Page not found');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          child: RaisedButton(
              onPressed: ()=>Navigator.of(context).pushNamed(RouteNames.test),
              child: Text(
                  'Отладочный экран',
                  style: TextStyle(fontSize: 20)
              )
          )
      ),
      body: _pageByIndex(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Расписание'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            title: new Text('Программы'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('Упражнения'))
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class TabScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabScreenState();
  }
}
