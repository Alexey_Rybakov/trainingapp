import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

///To generate models.g.dart run:
///    flutter packages pub run build_runner build --verbose

@JsonSerializable()
class Exercise {
  int id;
  String name;
  String description;
  String category;
  String image;

  Exercise(this.id, this.name, this.description, this.category, {this.image});

  factory Exercise.fromJson(Map<String, dynamic> json) =>
      _$ExerciseFromJson(json);

  Map<String, dynamic> toJson() => _$ExerciseToJson(this);
}

//Упражнение в рамках тренировки
//Ссылка на упражнениие из списка и упражнений и рабочие подходы
@JsonSerializable()
class WorkExercise {
  int id;
  Exercise exercise;

  //Текстовое описание количества подходов и повторений и т.д.
  String strategy;

  List<WorkSet> sets;

  WorkExercise(this.id, this.strategy, this.exercise, this.sets);

  factory WorkExercise.fromJson(Map<String, dynamic> json) =>
      _$WorkExerciseFromJson(json);

  Map<String, dynamic> toJson() => _$WorkExerciseToJson(this);
}

//Сет
@JsonSerializable()
class WorkSet {
  int id;
  int reps;
  double weight;

  WorkSet(this.id, this.reps, this.weight);

  factory WorkSet.fromJson(Map<String, dynamic> json) =>
      _$WorkSetFromJson(json);

  Map<String, dynamic> toJson() => _$WorkSetToJson(this);
}

//Тренировка
@JsonSerializable()
class WorkoutDay {
  int id;
  int time; //timestamp
  List<WorkExercise> workExercises;
  String title;
  bool finished;

  WorkoutDay(this.id, this.title, this.time, this.workExercises, {this.finished = false});

  factory WorkoutDay.fromJson(Map<String, dynamic> json) =>
      _$WorkoutDayFromJson(json);

  Map<String, dynamic> toJson() => _$WorkoutDayToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}

@JsonSerializable()
class Program {
  int id;
  String name;
  String description;

  List<WorkExercise> workExercises;

  Program(this.id, this.name, this.description, this.workExercises);

  Program.clone(Program object,
      {String id,
      String name,
      String description,
      List<WorkExercise> workExercises})
      : this(
            id ?? object.id,
            name ?? object.name,
            description ?? object.description,
            workExercises ?? object.workExercises);

  factory Program.fromJson(Map<String, dynamic> json) =>
      _$ProgramFromJson(json);

  Map<String, dynamic> toJson() => _$ProgramToJson(this);
}

@JsonSerializable()
class ProgramDay {
  int name;
  List<WorkExercise> workExercises;

  ProgramDay(this.name, this.workExercises);

  factory ProgramDay.fromJson(Map<String, dynamic> json) =>
      _$ProgramDayFromJson(json);

  Map<String, dynamic> toJson() => _$ProgramDayToJson(this);
}
