import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/presentation/exercises/ExerciseController.dart';
import 'package:training/presentation/navigation.dart';

class ExerciseListState extends State<ExerciseListScreen> {
  ExerciseController _exerciseController;

  final bool isPicker;

  ExerciseListState(this.isPicker);

  @override
  void initState() {
    _exerciseController = ExerciseController(Repository());
    _exerciseController.load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget._title),
        ),
        body: StreamBuilder<ExerciseState>(
            stream: _exerciseController.state,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text('${snapshot.error}');
              } else if (snapshot.hasData) {
                if (snapshot.data.loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (snapshot.data.data != null) {
                  var list = snapshot.data.data;
                  return Stack(
                    children: <Widget>[
                      ListView.builder(
                        itemCount: list.length,
                        itemBuilder: (context, index) {
                          var item = list[index];
                          return exerciseListItem(
                              item.name, item.description, () {
                                if (isPicker){
                                  Navigator.pop(context, item.id);
                                }else{
                                  Navigator.of(context).pushNamed(RouteNames.detailed, arguments: item);
                                }
                          });
                        },
                      )
                    ],
                  );
                }
              } else {
                return Container();
              }
            }));
  }
}

Widget exerciseListItem(String title, String subtitle, onClick()) {
  var isCheckboxVisible = false;
  Widget trailing;
  if (isCheckboxVisible) {
    trailing = Checkbox(value: true, onChanged: (bool value) => {});
  }
  return ListTile(
      isThreeLine: false,
      onTap: onClick,
      enabled: true,
      leading: Image.asset('assets/graphics/stub.png', height: 48, width: 48),
      trailing: trailing,
      title: Text(title, maxLines: 1),
      subtitle: Text(subtitle, maxLines: 2));
}

class ExerciseListScreen extends StatefulWidget {

  final String _title;

  ExerciseListScreen(this._title);

  @override
  State<StatefulWidget> createState() {
    return ExerciseListState(false);
  }
}

//Should return selected exercise id or nothing
class ExerciseListPickerScreen extends ExerciseListScreen{

  ExerciseListPickerScreen(): super('Выбор упражнения');

  @override
  State<StatefulWidget> createState() {
    return ExerciseListState(true);
  }

}
