import 'package:flutter/material.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/domain/models.dart';
import 'package:training/main.dart';
import 'package:training/presentation/exercise/ExerciseDetailed.dart';
import 'package:training/presentation/main/TabScreen.dart';
import 'package:training/presentation/program/Program.dart';
import 'package:training/presentation/program/schedule_program/ScheduleProgram.dart';
import 'package:training/presentation/workexercise/WorkExercise.dart';
import 'package:training/presentation/workout/Workout.dart';
import 'package:training/presentation/workout/enter_exe/WorkoutInput.dart';
import 'package:training/test/TestScreen.dart';


class RouteNames{
  static const String test = '/test';
  static const String detailed = '/exersises/detailed';
  static const String detailed2 = '/exersises/detailed';
  static const String detailed3 = '/exersises/detailed';
  static const String program = '/program';
  static const String scheduleProgram = '/program/scheduleProgram';
  static const String workExercise = '/program/workExercise';
  static const String workout = '/schedule/workout';
  static const String enterExe = '/schedule/workout/enter_exe';
}

RouteFactory mainRoute = (settings) {
  return MaterialPageRoute(builder: (context) => _createRoute(settings));
};

Widget _createRoute(RouteSettings settings) {
  switch (settings.name) {
    case "/":
      return TabScreen();
    case RouteNames.test:
      return TestScreen();
    case "/qwerty":
      return MyHomePage();
    case RouteNames.detailed:
      return ExerciseDetailedScreen(settings.arguments as Exercise);
    case RouteNames.program:
      return ProgramScreen(settings.arguments as Program);
    case RouteNames.workExercise:
      return WorkExerciseScreen(settings.arguments as WorkExercise);
    case RouteNames.workout:
      return WorkoutScreen(settings.arguments as LocalDate);
    case RouteNames.scheduleProgram:
      return ScheduleProgramScreen(settings.arguments as Program);
    case RouteNames.enterExe:
      return WorkoutInputScreen(settings.arguments as WorkoutInputArgument);
  }

  return null;
}
