import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:time_machine/time_machine.dart';
import 'package:training/DateTimeUtils.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';
import 'package:training/presentation/schedule/CalendarScrollCalculator.dart';

import 'ScheduleController.dart';

enum SType { COMPLETED, PLANNED, MISSED }

SType typeFromWorkoutDay(WorkoutDay day, int nowTime) {
  if (day.finished) {
    return SType.COMPLETED;
  } else if (day.time < nowTime) {
    return SType.MISSED;
  } else {
    return SType.PLANNED;
  }
}

class ScheduleState
    extends ScreenStateWithController<ScheduleController, ScheduleScreen> {
  double monthHeaderHeight = 40;
  double weekHeight = 32;

  ScrollController scrollController;

  ScrollCalculator _scrollCalculator;

  static LocalDate now = LocalDateTime.now().calendarDate;

  LocalDate from = now.subtractMonths(3);
  LocalDate to = now.addMonths(3);

  @override
  void initState() {
    _scrollCalculator =
        ScrollCalculator(monthHeaderHeight, weekHeight, from, to);
    scrollController = ScrollController(
        initialScrollOffset: _scrollCalculator.calculateScrollToMonth(3));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Расписание'),
            bottom: PreferredSize(
                preferredSize: const Size.fromHeight(16.0),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Center(
                          child: Text('пн',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('вт',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('ср',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('чт',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('пт',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('сб',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                      Center(
                          child: Text('вс',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white))),
                    ],
                  ),
                ))),
        floatingActionButton: FloatingActionButton(
            child: new Icon(Icons.today),
            onPressed: () => {
                  scrollController
                      .jumpTo(_scrollCalculator.calculateScrollToMonth(3))
                }),
        body: StreamBuilder(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<ScheduleModel> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<ScheduleModel>) {
                  var data = result.data;
                  var nowTime = Instant.now().epochMilliseconds;
                  return calendarView(from, to, now, (date) {
                    WorkoutDay day = data.daysByDate[date];
                    if (day == null) return false;
                    return typeFromWorkoutDay(day, nowTime) == SType.MISSED;
                  }, (date) {
                    WorkoutDay day = data.daysByDate[date];
                    if (day == null) return false;
                    return typeFromWorkoutDay(day, nowTime) == SType.PLANNED;
                  }, (date) {
                    WorkoutDay day = data.daysByDate[date];
                    if (day == null) return false;
                    return typeFromWorkoutDay(day, nowTime) == SType.COMPLETED;
                  }, (date) => now.equals(date));
                }
              }
              return Container();
            }));
  }

  @override
  ScheduleController provideController() {
    return ScheduleController(Repository());
  }

  Widget calendarView(
      LocalDate from,
      LocalDate to,
      LocalDate currentDate,
      DatePredicate missed,
      DatePredicate planned,
      DatePredicate completed,
      DatePredicate today) {
    return ListView.builder(
        itemCount: 300,
        controller: scrollController,
        itemBuilder: (BuildContext ctxt, int index) {
          final monthDate = from.addMonths(index);
          return month(monthDate, missed, planned, completed, today);
        });
  }

  Widget month(LocalDate monthDate, DatePredicate missed, DatePredicate planned,
      DatePredicate completed, DatePredicate today) {
    final List<List<LocalDate>> month = weeklyMonthOf(monthDate);
    final List<Widget> dayRows = month
        .map((weekDates) => week(weekDates, missed, planned, completed, today))
        .toList();
    return Column(
      children: [header(monthDate)]..addAll(dayRows),
    );
  }

  Widget header(LocalDate monthDate) {
    return SizedBox(
        child: Center(
          child: Text(
            DateFormat.yMMM().format(monthDate.toDateTimeUnspecified()),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        height: monthHeaderHeight);
  }

  Widget week(List<LocalDate> weekDates, DatePredicate missed,
      DatePredicate planned, DatePredicate completed, DatePredicate today) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: weekDates
            .map((date) => InkWell(
                  borderRadius: BorderRadius.circular(100),
                  onTap: () => {controller.onDayClicked(date)},
                  child: SizedBox(
                      child: Stack(
                        children: [
                          Center(
                              child: Text(
                            '${date.dayOfMonth}',
                            textAlign: TextAlign.center,
                          )),
                          if (today(date))
                            SizedBox(
                              width: weekHeight,
                              height: weekHeight,
                              child: DecoratedBox(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          width: 1, color: Colors.blue))),
                            ),
                          if (missed(date))
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 6,
                                      height: 6,
                                      child: DecoratedBox(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: red)),
                                    )
                                  ]),
                            ),
                          if (completed(date))
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 12,
                                      height: 12,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 2),
                                        child: new  Icon(Icons.check,
                                            color: green, size: 12),
                                      ),
                                    )
                                  ]),
                            ),
                          if (planned(date))
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 6,
                                      height: 6,
                                      child: DecoratedBox(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: blue)),
                                    )
                                  ]),
                            )
                        ],
                      ),
                      width: weekHeight,
                      height: weekHeight),
                ))
            .toList());
  }
}

typedef bool DatePredicate(LocalDate date);

class ScheduleScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScheduleState();
  }
}
