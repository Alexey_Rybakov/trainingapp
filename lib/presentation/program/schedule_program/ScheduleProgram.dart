import 'package:flutter/material.dart';
import 'package:time_machine/time_machine.dart';
import 'package:toast/toast.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
class ScheduleProgramState extends State<ScheduleProgramScreen> {
  LocalDate started = LocalDate.today();

  TextEditingController _repeatEditingController =
      TextEditingController(text: '7');
  TextEditingController _trainCountEditor = TextEditingController(text: '4');

  bool isProgress = false;

  @override
  Widget build(BuildContext context) {
    if (isProgress) {
      return Scaffold(
        body: Container(
            decoration: BoxDecoration(color: Colors.white),
            alignment: Alignment(0.0, 0.0),
            child: CircularProgressIndicator()),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text('Планирование тренировок'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                schedule();
              },
              tooltip: 'Запланировать',
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 16, bottom: 8),
              child: Text(
                'Начиная с:',
                textAlign: TextAlign.left,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16),
              child: FlatButton(
                  child: Text(started.toString()),
                  onPressed: () {
                    pickDate();
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: TextField(
                keyboardType: TextInputType.number,
                obscureText: false,
                controller: _repeatEditingController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Повторять через (Дней)'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: TextField(
                keyboardType: TextInputType.number,
                obscureText: false,
                controller: _trainCountEditor,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Количество тренировок'),
              ),
            )
          ],
        ),
      );
    }
  }

  void pickDate() async{
    DateTime date = await showDatePicker(
        builder: (context, widget) {
          return FittedBox(
            child: Theme(
              child: widget,
              data: ThemeData(),
            ),
          );
        },
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2020));
    setState(() {
      started = LocalDate.dateTime(date);
    });
  }

  void setError(String error){
    setState(() {
      isProgress = false;
      Toast.show(error, context, duration: Toast.LENGTH_LONG);

    });
  }
  void schedule() async {

    String repeates = _repeatEditingController.value.text;
    int repeatesNum = int.tryParse(repeates);
    if(repeatesNum == null){
      setError('Укажите период повторений');
      return;
    }else if(repeatesNum > 30){
      setError('Период повторений не более 30 дней');
      return;
    }

    String count = _trainCountEditor.value.text;
    int countNum = int.tryParse(repeates);
    if(repeatesNum == null){
      setError('Укажите количество тренировок');
      return;
    }else if(repeatesNum > 10){
      setError('Количество не более 10');
      return;
    }

    setState(() {
      isProgress = true;
    });

    Repository repository = Repository();
    Program program = widget.program;

    var currentDate = started;

    for(var i = 0; i < countNum; i++){
      currentDate = started.addDays(repeatesNum * i);
      int time = currentDate.atMidnight().inZoneLeniently(DateTimeZone.local).toInstant().epochMilliseconds;
      await repository.addWorkoutDay(
          WorkoutDay(null, program.name, time, program.workExercises)
      );
    }

    Toast.show('Тренировки запланированы', context, duration: Toast.LENGTH_LONG);
    Navigator.of(context).pop();
  }
}

class ScheduleProgramScreen extends StatefulWidget {

  Program program;

  ScheduleProgramScreen(this.program);

  @override
  State<StatefulWidget> createState() {
    return ScheduleProgramState();
  }
}
