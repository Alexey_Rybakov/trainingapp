import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

import 'WorkoutInput.dart';

class WorkoutInputController extends LoadingController<WorkExercise> {
  Repository _repository;
  WorkoutInputArgument argument;

  WorkoutInputController(this._repository, this.argument);

  @override
  void onCreate() async {
    super.onCreate();
  }

  ///Update mutated WorkExercise into WorkoutDay
  void updateWorkExercise(WorkExercise workExercise) async {
    var exe = argument.workExercise;
    exe.id = workExercise.id;
    exe.exercise = workExercise.exercise;
    exe.sets = workExercise.sets;
    exe.strategy = workExercise.strategy;

    stateSubject.add(AsyncResult.data(exe));

    await _repository.updateWorkoutDay(argument.workoutDay);
  }

  @override
  Stream<WorkExercise> dataStream() {
    return Stream.fromIterable([argument.workExercise]);
  }

  void removeSet(WorkSet set) async {
    var exe = argument.workExercise;
    exe.sets.remove(set);
    updateWorkExercise(exe);
  }

  void updateSet(String reps, String weight, WorkSet set) {
    var repsNum = int.tryParse(reps);
    var weightNum = double.tryParse(weight);

    if (repsNum != null && weightNum != null) {
      var exe = argument.workExercise;
      set.weight = weightNum;
      set.reps = repsNum;
      updateWorkExercise(exe);
    }
  }

  WorkSet createNewSet(){
    WorkSet workSet = WorkSet(null, 0, 0);
    argument.workExercise.sets.add(workSet);
    updateWorkExercise(argument.workExercise);
    return workSet;
  }
}
