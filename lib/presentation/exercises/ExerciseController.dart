import 'dart:async';

import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:rxdart/rxdart.dart';

class ExerciseState {

  bool loading = false;

  List<Exercise> data;

  ExerciseState.data(this.data);

  ExerciseState.loading() : loading = true;
}

class ExerciseController {

  BehaviorSubject<ExerciseState> _stateSubject = BehaviorSubject.seeded(ExerciseState.loading());

  Stream<ExerciseState> get state => _stateSubject;

  Repository _repository;

  ExerciseController(this._repository);

  void load() async {
    _stateSubject.add(ExerciseState.loading());
    _stateSubject.addStream(_repository.getExercises().asStream().map((exercises) {
      return ExerciseState.data(exercises);
    }));
  }
}
