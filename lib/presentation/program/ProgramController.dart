import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';

class ProgramController extends LoadingController<Program> {
  Repository _repository;

  int programId;

  ProgramController(this._repository, this.programId);

  @override
  Stream<Program> dataStream() {
    return _repository.getProgramById(programId).asStream();
  }

  void onExerciseReordered(int oldIndex, int newIndex) async {
    final snapshot = await state.first;
    if (snapshot is Data<Program>) {
      final program = snapshot.data;
      List<WorkExercise> list = List.of(program.workExercises);
      list.insert(newIndex, list.removeAt(oldIndex));
      stateSubject
          .add(AsyncResult.data(Program.clone(program, workExercises: list)));
    }
  }

  void addExercise(int id) async {
    Program data = await dataStream().first;
    Exercise e = await _repository.getExerciseById(id);

    data.workExercises.add(WorkExercise(null, '3*12 среднее напряжение', e, []));
    stateSubject.add(AsyncResult.data(data));
    await _repository.updateProgram(data);
  }
}
