import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/navigation.dart';
import 'package:training/presentation/programs/ProgramsListController.dart';

class ProgramsListState extends ScreenStateWithController<
    ProgramsListController, ProgramsListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Тренировочне дни'),
        ),
        /*floatingActionButton: FloatingActionButton(
          onPressed: () =>
              {Navigator.of(context).pushNamed(RouteNames.program)},
          tooltip: 'Создать программу',
          child: Icon(Icons.add),
        ),*/
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        body: StreamBuilder<AsyncResult<List<Program>>>(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<List<Program>> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<List<Program>>) {
                  var data = result.data;
                  return content(data);
                }
              }
              return Container();
            }));
  }

  Widget content(List<Program> data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
      return item(data[index]);
    });
  }

  Widget item(Program item) {
    return ListTile(
        key: ObjectKey(item.id),
        isThreeLine: false,
        //trailing: Icon(Icons.drag_handle),
        onTap: () => {Navigator.of(context).pushNamed(RouteNames.program, arguments: item)},
        enabled: true,
        title: Text(item.name, maxLines: 1),
        subtitle: Text(item.description, maxLines: 2));
  }

  @override
  ProgramsListController provideController() {
    return ProgramsListController(Repository());
  }
}

/* Widget content(List<Program> data) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          header('Описание'),
          Text(''),
        ],
      ),
    );
  }
  Widget header(String text){
    return Text(
      'Описание'.toUpperCase(),
      style: TextStyle(color: categoryColor),
    );
  }*/

class ProgramsListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProgramsListState();
  }
}
