import 'package:flutter/material.dart';
import 'package:training/data/Repository.dart';
import 'package:training/domain/models.dart';
import 'package:training/presentation/base/Controller.dart';
import 'package:training/presentation/colors.dart';
import 'package:training/presentation/workexercise/WorkExerciseController.dart';

class WorkExerciseState extends ScreenStateWithController<
    WorkExerciseController, WorkExerciseScreen> {
  @override
  WorkExerciseController provideController() {
    return WorkExerciseController(Repository(), widget.workExercise);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Упражнение'),
        ),
        body: StreamBuilder<AsyncResult<WorkExercise>>(
            stream: controller.state,
            builder: (context, snapshot) {
              AsyncResult<WorkExercise> result = snapshot.data;
              if (snapshot.hasData) {
                if (result is Loading) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: Center(child: CircularProgressIndicator()));
                } else if (result is Error) {
                  return Text('${snapshot.error}');
                } else if (result is Data<WorkExercise>) {
                  var data = result.data;
                  return content(data);
                }
              }
              return Container();
            }));
  }

  Widget content(WorkExercise data) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                data.exercise.name,
                style: Theme.of(context).textTheme.title,
                textAlign: TextAlign.start,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Стратегия',
                  style: TextStyle(color: categoryColor),
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft, child: Text(data.strategy)),
          ),
        ],
      ),
    );
  }

  Table table() {
    return Table(
        border: TableBorder.all(
            color: Colors.blue, width: 1.0, style: BorderStyle.solid),
        children: [
          TableRow(children: [
            TableCell(
              child: Text('№'),
            ),
            TableCell(
              child: Text('Вес, кг'),
            ),
            TableCell(
              child: Text('Повторения'),
            )
          ])
        ]);
  }

/*TableRow tableRow(WorkSet workSet) {

    data.sets
        .map((WorkSet element) => tableRow(element))
    )
  }*/
}

class WorkExerciseScreen extends StatefulWidget {
  final WorkExercise workExercise;

  WorkExerciseScreen(this.workExercise);

  @override
  State<StatefulWidget> createState() {
    return WorkExerciseState();
  }
}
