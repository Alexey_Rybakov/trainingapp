import 'dart:async';

import 'package:time_machine/time_machine.dart';
import 'package:training/data/Database.dart';
import 'package:training/domain/models.dart';

class Repository {
  SqlDatabase database = SqlDatabase();

  ///Тренировки прошедшие и запланированные
  Future<List<WorkoutDay>> workoutDays({LocalDate date}) async {
    //LocalDate today = LocalDateTime.now().calendarDate;
    return await database.workoutDays(localDate: date);
  }

  Future updateWorkoutDay(WorkoutDay workoutDay) async {
      return await database.updateWorkoutDay(workoutDay);
  }
  Future addWorkoutDay(WorkoutDay workoutDay) async{
    return await database.addWorkoutDay(workoutDay);
  }

  Future<Program> getProgramById(int id) async {
    List<Program> programs = await getPrograms();
    return programs.firstWhere((Program program) => program.id == id);
  }

  Future<List<Program>> getPrograms() async {
    return await database.programs();
  }
  Future updateProgram(Program program) async{
    return await database.updateProgram(program);
  }

  Future<WorkExercise> getWorkExerciseById(int id) async {
    List<Program> all = await getPrograms();
    return all
        .firstWhere((Program element) => element.id == id)
        .workExercises
        .firstWhere((WorkExercise element) => element.id == id);
  }

  Future<Exercise> getExerciseById(int id) async {
    List<Exercise> all = await getExercises();
    return all.firstWhere((Exercise element) => element.id == id);
  }

  Future<List<Exercise>> getExercises() async {
    /*List<Map<String, dynamic>> ex = await database.listOfExercises();

      List<Exercise> list = ex.map<Exercise>((map){
        return Exercise.fromJson(map);
      }).toList();*/
    return [
      Exercise(
          1,
          "Жим лежа",
          "Описание длинное для жима лежа",
          "category"),
      Exercise(2, "Тяга штанги в наклоне", "Описание", "category"),
      Exercise(3, "Подъем штанги на бицепс", "Описание", "category"),
      Exercise(4, "Приседания со штангой", "Описание", "category"),
      Exercise(5, "Разгибания ног сидя", "Описание", "category")
    ];
  }
}
